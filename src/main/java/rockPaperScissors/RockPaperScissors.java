package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */

        new RockPaperScissors().run();

    }
    

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String computer() {

        String text = "";
        Random rand = new Random();
        String randomElement = rpsChoices.get(rand.nextInt(rpsChoices.size()));
        text += randomElement;
        return text;
    }

    public String player() {

        System.out.println("Your choice (Rock/Paper/Scissors)?");
        String humanChoice = sc.nextLine();
        System.out.print("Human chose " + humanChoice + ",");
        return humanChoice;

    }


    public void run() {
        // TODO: Implement Rock Paper Scissors

        Object igjen = true;

        while(igjen.equals(true)) {

            System.out.println("Let's play round " + roundCounter);

            String humanChoice = player();

            String randomChoice = computer();
            System.out.print(" computer chose " + randomChoice + ".");

            if (humanChoice.equals(randomChoice)) {
                System.out.println(" It's a tie!");
            } else if (humanChoice.equals("rock") && randomChoice.equals("scissors")) {
                System.out.println(" Human wins!");
                humanScore += 1;
            } else if (humanChoice.equals("scissors") && randomChoice.equals("paper")) {
                System.out.println(" Human wins!");
                humanScore += 1;
            } else if (humanChoice.equals("paper") && randomChoice.equals("rock")) {
                System.out.println(" Human wins!");
                humanScore += 1;
            }
            else if (randomChoice.equals("rock") && humanChoice.equals("scissors")) {
                System.out.println(" Computer wins!");
                computerScore += 1;
            } else if (randomChoice.equals("scissors") && humanChoice.equals("paper")) {
                System.out.println(" Computer wins!");
                computerScore += 1;
            } else if (randomChoice.equals("paper") && humanChoice.equals("rock")) {
                System.out.println(" Computer wins!");
                computerScore += 1;
            } else {
                System.out.println(" I do not understand " + humanChoice + ". Could you try again?");
            }

            System.out.println("Score: human " + humanScore + ", " + "computer " + computerScore);

            System.out.println("Do you wish to continue playing? (y/n)?");
            String la = sc.nextLine();
            if (la.equals("y")) {
                igjen = true;
                roundCounter += 1;
            } else {
                igjen = false;
                System.out.print("Bye bye :)");
            }

        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
